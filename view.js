
// utils
var build = function (color) {
    var view=document.getElementById('box1');
    view.style.backgroundColor=color;
    view.style.border="1px";
    view.style.height="300px";
    view.style.width="300px";
    return view;
}

function View(color) {
    this.color = color;
    this.Viewelement= build(color);
}

View.prototype.update=function(color){
    this.Viewelement.style.backgroundColor=color;
}
