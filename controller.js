function Controller(view,model){
    this.view=view;
    this.model=model;
    this.update = function (newColor) {
        this.model.notify(newColor);
    };
}
function create(color) {
   // var color = "blue";
    var firstView = new View(color);
    var model = new Model("blue");
    model.subscribe(firstView);
    var controller=new Controller(firstView,model);
    // document.getElementById("change").addEventListener("click", function(){
    //     var color = $("input[name='color']:checked").val();
    //     model.notify(color);
    //   });
    return controller;
}
