function Model(color)
{
    this.currentColor=color;
    this.subscriberList=[];
}

Model.prototype.subscribe=function(view) {
    this.subscriberList.push(view);
}

Model.prototype.notify= function(color) {
    this.subscriberList[0].update(color);
}